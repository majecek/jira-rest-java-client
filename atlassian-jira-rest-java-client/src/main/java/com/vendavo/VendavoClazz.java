package com.vendavo;

import com.atlassian.jira.rest.client.JiraRestClient;
import com.atlassian.jira.rest.client.NullProgressMonitor;
import com.atlassian.jira.rest.client.ProgressMonitor;
import com.atlassian.jira.rest.client.domain.*;
import com.atlassian.jira.rest.client.internal.jersey.JerseyJiraRestClientFactory;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import javax.naming.AuthenticationException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class VendavoClazz {

	private String fixVersion;
	private boolean isSubtask;
	private String JQL;
	private String url;
	private String username;
	private String password;
	private String assignee;
    private boolean showOnlyFoundJiras;

    public boolean isShowOnlyFoundJiras() {
        return showOnlyFoundJiras;
    }

    public void setShowOnlyFoundJiras(boolean showOnlyFoundJiras) {
        this.showOnlyFoundJiras = showOnlyFoundJiras;
    }

	public String getAssignee() {
		return assignee;
	}
	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFixVersion() {
		return fixVersion;
	}
	public void setFixVersion(String fixVersion) {
		this.fixVersion = fixVersion;
	}
	public boolean isSubtask() {
		return isSubtask;
	}
	public void setSubtask(boolean isSubtask) {
		this.isSubtask = isSubtask;
	}
	public String getJQL() {
		return JQL;
	}
	public void setJQL(String jQL) {
		JQL = jQL;
	}


	/**
	 * create new issue
	 * @return collection with list of newly created issues
	 * @throws URISyntaxException
	 * @throws AuthenticationException
	 */
	public Collection<String> createIssue() throws URISyntaxException, AuthenticationException {
		Collection<String> result = new ArrayList<String>();
		
		JerseyJiraRestClientFactory f = new JerseyJiraRestClientFactory();
		JiraRestClient jc = f.createWithBasicHttpAuthentication(new URI(getUrl()), getUsername(), getPassword());
		ProgressMonitor pm = new NullProgressMonitor();
		SearchResult r = jc.getSearchClient().searchJql(getJQL(), null);

        if(isShowOnlyFoundJiras()) {
            result.add("Total found jiras: " + Integer.toString(r.getTotal()) + "\n");
            result.add("Issues: " + r.getIssues().toString());
            System.out.println(result);
            return result;
        }

        for (BasicIssue basicIssue : r.getIssues()) {
            Issue issue = jc.getIssueClient().getIssue(basicIssue.getKey(), null);
            Iterable<Version> versions = jc.getIssueClient().getVersions(issue.getProject().getKey(), pm);
            if (checkVersion(getFixVersion(), versions)) {
                try {
                    JSONObject json = createJSONFromIssue(issue, isSubtask(), getFixVersion());
                    String issueID = jc.getIssueClient().createIssue(pm, json);
                    jc.getIssueClient().updateAssigee(pm, getAssignee(), issueID);
                    result.add(issueID);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("FixVersion [" + getFixVersion() + "] doesn't exists for project: " + issue.getProject().getKey());
                throw new IllegalArgumentException("FixVersion [" + getFixVersion() + "] doesn't exists for project: " + issue.getProject().getKey());
            }
        }
		return result;
	}
	
	
	/**
	 * check whether version exists in given project
	 * @param fixVersion
	 * @param versions
	 * @return
	 */
	private boolean checkVersion(String fixVersion, Iterable<Version> versions) {
		for (Version version : versions) {
			if(version.getName().equals(fixVersion)) return true;
		}
		return false;
	}
	
	
	/**
	 * Create JSON object for given issue
	 * @param issue
	 * @param subtask
	 * @param version
	 * @return
	 * @throws JSONException
	 * @throws org.codehaus.jettison.json.JSONException 
	 */
	private JSONObject createJSONFromIssue(Issue issue, Boolean subtask, String version) throws org.codehaus.jettison.json.JSONException {
		String name = "name";
		String key = "key";
		String property = "project";
		String description = "description";
		String summary = "summary";
		String components = "components";
		String fixVersions = "fixVersions";
		String parent = "parent";
		String issuetype = "issuetype";
		String fields = "fields";
		String subtaskString = "Sub-task";
		String bug = "Bug";
		
		JSONObject json = new JSONObject();
		JSONObject fieldsJS = new JSONObject();
		JSONObject projectJS = new JSONObject();
		JSONObject issuetypeJS = new JSONObject();
		JSONObject parentJS = new JSONObject();
		JSONObject componentsJS = new JSONObject();
		JSONArray componentsJSArray = new JSONArray();
		JSONArray versionJSArray = new JSONArray();
		JSONObject versionsJS = new JSONObject();
		
		
		componentsJS.put(name, ((BasicComponent) issue.getComponents().iterator().next()).getName());
		componentsJSArray.put(componentsJS);
		projectJS.put(key, issue.getProject().getKey());
		versionsJS.put(name, version);
		versionJSArray.put(versionsJS);
		
		fieldsJS.put(property, projectJS);
		fieldsJS.put(description, "See the parent task for description.");
		String summaryString = issue.getSummary();
		if (summaryString.length() > 240) {
			summaryString = summaryString.substring(0, 240);
		}
		fieldsJS.put(summary, "port to "+ version +": " + summaryString);
		
		if (subtask) {
			issuetypeJS.put(name, subtaskString);
			parentJS.put(key, issue.getKey());
			fieldsJS.put(parent, parentJS);
		} else {
			issuetypeJS.put(name, bug);
		}
		
		fieldsJS.put(issuetype, issuetypeJS);
		fieldsJS.put(components, componentsJSArray);
		fieldsJS.put(fixVersions, versionJSArray);
		
		json.put(fields, fieldsJS);
		return json;
	}
	


	/**
	 * @param args
	 * @throws URISyntaxException 
	 * @throws JSONException 
	 * @throws AuthenticationException
	 * @throws IOException 
	 */
	public static void main(String[] args) throws URISyntaxException, AuthenticationException, IOException {
		long a = System.currentTimeMillis();
		VendavoClazz  vendavoJira = new VendavoClazz();
		System.out.print("Enter your query: ");
		String query = (new BufferedReader(new InputStreamReader(System.in))).readLine().replaceAll("\"","'");

		vendavoJira.setPassword(AppProperties.getInstance().getJiraPassword());
		vendavoJira.setUsername(AppProperties.getInstance().getJiraUser());
		vendavoJira.setUrl(AppProperties.getInstance().getJiraURL());


        // true = will show just results of the query we have entered and will not
        //create any new jira or subtasks
        // false = will create new jiras or subtasks

        System.out.print("Testing only ? y/n: ");
        String testing = (new BufferedReader(new InputStreamReader(System.in))).readLine().replaceAll("\"","'");
        boolean testingB = testing.equals("y") ? Boolean.TRUE : Boolean.FALSE;
        vendavoJira.setShowOnlyFoundJiras(testingB);

        if (!testingB) {
            System.out.print("Set FixVersion for subtasks: ");
            String fixVersionSubtasks = (new BufferedReader(new InputStreamReader(System.in))).readLine().replaceAll("\"","'");
            vendavoJira.setFixVersion(fixVersionSubtasks);
            vendavoJira.setSubtask(true);
            System.out.print("Assignee = "+AppProperties.getInstance().getJiraUser() + " y/n:");
            String user = (new BufferedReader(new InputStreamReader(System.in))).readLine().replaceAll("\"","'");
            if (user.equals("n")) {
                System.out.print("New assignee name: "+AppProperties.getInstance().getJiraUser() );
                 user = (new BufferedReader(new InputStreamReader(System.in))).readLine().replaceAll("\"","'");
                vendavoJira.setAssignee(user);
            } else {
                vendavoJira.setAssignee(AppProperties.getInstance().getJiraUser());
            }
        }

        vendavoJira.setJQL(query);

		Collection<String> result = vendavoJira.createIssue();
		long b = System.currentTimeMillis() - a;

        if (!testingB) {
            StringBuilder jql = new StringBuilder();
            jql.append("key in (").append(result).append(")");
            jql.deleteCharAt(jql.indexOf("[")).deleteCharAt(jql.indexOf("]"));

            System.out.println("Created Issues JQL: " + jql);
            System.out.println("milsec:" + b + "  min:" +b/1000/60);
        }

		
		
	}

}
